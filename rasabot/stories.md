## Generated Story 5352666409542250850
* greet
    - utter_greet
* inform{"location": "ranchi"}
    - slot{"location": "ranchi"}
    - action_weather
    - slot{"location": "ranchi"}
* inform{"location": "london"}
    - slot{"location": "london"}
    - action_weather
    - slot{"location": "london"}
    - export

## Generated Story -29958860466619411
* greet
    - utter_greet
* inform{"location": "gaya"}
    - slot{"location": "gaya"}
    - action_weather
    - slot{"location": "gaya"}
* inform
    - action_weather
    - slot{"location": "gaya"}
* inform{"location": "london"}
    - slot{"location": "london"}
    - action_weather
    - slot{"location": "london"}
* inform{"location": "italy"}
    - slot{"location": "italy"}
    - action_weather
    - slot{"location": "italy"}
* inform
* inform
    - utter_ask_location
* inform
* inform
    - action_weather
    - slot{"location": "italy"}
* goodbye
    - utter_goodbye
    - export

