from rasa_core.actions.action import Action
from rasa_core.events import SlotSet


class ActionWeather(Action):
    def run(self, dispatcher, tracker, domain):
        """
        Run the action to get the weather:

        Fow now, just return dummy data
        :param dispatcher:
        :param tracker:
        :param domain:
        :return:
        """
        loc = tracker.get_slot('location')
        response = 'It is currently nice in {}'.format(loc)

        dispatcher.utter_message(response)
        return [SlotSet('location', loc)]

    def name(self):
        return 'action_weather'
