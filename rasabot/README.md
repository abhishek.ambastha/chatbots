# RASA Chatbot Example

This repo replicates the rasa chatbot tutorial, primarilry
for learning purposes. Thanks to Justina Petraityte for creating
the original tutorial.

## To Run
1. Create a model directory.
2. Run nlu_model.py . This mainly provides classification and entity
extraction
3. Run train_init.py . This trains the dialogue model with bootstrap data.
4. Run train_online.py. Trains the dialogue models interactively.
5. Run dialogue_management.py. Trains the dialogue model for final
deployment.

## Conclusions
For entity extraction and intent classification, rasa_nlu uses spacy,
tensorflow and sklearn libraries. Also, the nlu component can be used
for other NLP related tasks easily.